package stringClass;

public class MainStringBuilder {
    public static void main(String[] args) {
        String firstName = "Ahmad Roni";
        System.out.println("First Name "+ firstName);
        String lastName = "Purwanto";
        System.out.println("Last Name "+ firstName);

        String namaLengkap = firstName +" "+lastName;
        System.out.println("Nama Lengkap "+ namaLengkap);

        //untuk concat antara string lainnya gunakan STRING BUILDER agar safe thread
        StringBuilder builder = new StringBuilder(firstName)
                .append(" ").append(lastName);
        System.out.println("Builder "+ builder);

        StringBuffer buffer = new StringBuffer(firstName)
                .append(" ").append(lastName);
        System.out.println("Buffer "+ buffer);


    }

}
