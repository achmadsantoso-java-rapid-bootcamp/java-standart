package genericType;

public class Data {
    public Object data;

    public static void process (MyData<? extends Object> data){
        Object object = data.getData();
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
