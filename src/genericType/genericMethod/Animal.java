package genericType.genericMethod;

public class Animal {
    public static <T> String sayHello(T name){
        return "Halo saya adalah " +name;
    }
}
