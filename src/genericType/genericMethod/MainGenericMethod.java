package genericType.genericMethod;

public class MainGenericMethod {
    public static void main(String[] args) {
        String[] names = {"Eko", "Kurniawan", "Keren"};
        Integer[] angka = {1, 5, 9, 100, 73};

        System.out.println("Banyak data di array names : "+ArrayHelper.count(names));
        System.out.println("Banyak data di array angka : "+ArrayHelper.count(angka));

        System.out.println("\n");
        String namaHewan = "Kucing";
        System.out.println(Animal.sayHello(namaHewan));


    }
}
