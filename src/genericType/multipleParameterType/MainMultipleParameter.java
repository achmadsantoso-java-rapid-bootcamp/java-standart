package genericType.multipleParameterType;

public class MainMultipleParameter {
    public static void main(String[] args) {
        Pair<String, Integer> pair = new Pair<>("Eko", 23);
        System.out.println(pair.getFirst());
        System.out.println(pair.getSecond());

        System.out.println("\n");
        Pair<String, Integer> makanan1 = new Pair<>("Mie Tek-tek", 15000);
        System.out.println("Nama Makanan : "+makanan1.getFirst() + ", Harga : "+makanan1.getSecond());
    }
}
